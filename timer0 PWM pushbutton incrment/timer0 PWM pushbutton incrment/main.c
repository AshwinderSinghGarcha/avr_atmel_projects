/*
 * timer0 PWM pushbutton incrment.c
 *
 * Created: 10/29/2020 9:17:37 AM
 * Author : Developer
 */ 
///---------------------------------------------fast pwm ---- phase correct pwm ----------------
#include <avr/io.h>
#include <util/delay.h>


void PWM_setup();
void PWM_output(int ash);

void PWM_setup()
{  TCCR0A |= (1<<COM0B1)|(1<<COM0B0);
   TCCR0B |= (0<<WGM02)|(0<<CS02)|(0<<CS01)|(1<<CS00);             // NO PRESCALE 
  // TCCR0A |= (1<<COM0A1)|(0<<COM0A0)|(1<<WGM01)|(1<<WGM00);        // FAST PWM       0xff top, NON INVERTING OCOA ON PIN PD6
   TCCR0A |= (1<<COM0A1)|(0<<COM0A0)|(0<<WGM01)|(1<<WGM00);        // phase correct  oxff top, NON INVERTING OCOA ON PIN PD6
  //   TCCR0B |= (1<<WGM02);
  //   TCCR0A |= (1<<COM0A1)|(0<<COM0A0)|(1<<WGM01)|(1<<WGM00);        // Fast PWM       ocra top, NON INVERTING OCOA ON PIN PD6
 //  TCCR0A |= (1<<COM0A1)|(0<<COM0A0)|(0<<WGM01)|(1<<WGM00);        // phase correct  ocra top, NON INVERTING OCOA ON PIN PD6
   TCNT0  |= 0XFF;
}


void PWM_output(int a)
{
OCR0A = a;
}

void PWM_output1(int a)
{
	OCR0B = a;
}

int main(void)
{ int J=0;
  
 PWM_setup();
 
  DDRD|= (1<<6);           // green LED OUTPUT     pwm PIN TIMER T0         oc0a
  DDRD|= (1<<5);           //yellow led output     pwm pin timer0           oc0b
 
  DDRD&=~(1<<4);          // PB1 AS INPUT 
  PORTD|= (1<<4);         // PULLUP INPUT 
  
  DDRD&=~(1<<7);           // PB2 INPUT
  PORTD|=(1<<7);
 
    while (1) 
    { 
	  if ((PIND &(1<<4))== 0)                   // WHEN PB1 =0 
		{ if(J<255)
			 {   
				 PWM_output(J);
				 PWM_output1(J);
				 J=J + 1;
				_delay_ms(400);
				
		     } 
			 
	       if(J==255)
	         {   
				 J=255;
				 PWM_output(J);
				 PWM_output1(J);
	         }
		
		 } 
		
		if ((PIND &(1<<7))== 0)                   // WHEN PB2 =0 
		{ if(J>0)
			{
				PWM_output(J);
				PWM_output1(J);
				J=J - 1;
				_delay_ms(400);
				
			}
			
			if(J==0)
			{
				J=0;
				PWM_output(J);
				PWM_output1(J);
			}
			
		}
		
    }
}

