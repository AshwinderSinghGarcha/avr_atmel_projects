//------------------------------------------lcd-----------------------------------//
//====================== rs = 0 cmd , rs =1 data , e high to low pulse 450nanosec
#define F_CPU 8000000ul
#include <avr/io.h>
#include <util/delay.h>
//#define rs 2
//#define en 3
//#define datapin PORTD
#define LCD_Dir  DDRD     /* Define LCD data port direction */
#define datapin PORTD      /* Define LCD data port */
#define rs 2        /* Define Register Select pin */
#define en 3
//#define cmdpin PORTD
/*
void send_cmd(unsigned char cmd);
void send_data(unsigned char data);
void lcd_intil(void);
void lcd_Srint(char * str);
void set_cursor(unsigned char x,unsigned char y,char * str);
void lcd_clear(void);*/

//----------------------data pins -----------------------//
//--------- pb2 pb3 pb4 pb5------mappto
//--------  d4  d5  d6  d7--------------
//-----------------controlling pins-----------------
//----------enable rs  map to pd2 pd3----------





void send_cmd(unsigned char cmd)
{
	datapin=(datapin & 0x0F)|(cmd & 0xF0);               // high nibble goes first
	datapin&=~(1<<rs);                 // rs = 0
	datapin|=(1<<en);                 // en high
	_delay_us(1);
	datapin&=~(1<<en);                 // en low
	_delay_us(200);
	
	datapin=(datapin & 0x0F)|(cmd<<4);                // low nibble
	datapin|=(1<<en);                 // en high
	_delay_us(1);
	datapin&=~(1<<en);                 // en low
	_delay_ms(2);
}
void send_data(unsigned char data)
{
	datapin=(datapin & 0x0F)|(data & 0xF0);               // high nibble goes first
	datapin|=(1<<rs);                 // rs = 1
	datapin|=(1<<en);                 // en high
	_delay_us(1);
	datapin&=~(1<<en);                 // en low
	_delay_us(200);
	
	datapin=(datapin & 0x0F)|(data<<4);                // low nibble
	datapin|=(1<<en);                 // en high
	_delay_us(1);
	datapin&=~(1<<en);                 // en low
	_delay_us(2);
}
void lcd_intil(void)
{
	//DDRD |=(1<<7)|(1<<6)|(1<<5)|(1<<4);
	//DDRB|=(1<<4)|(1<<5)|(1<<3)|(1<<2);
	//DDRD|=(1<<rs)|(1<<en);                      // make ouput port
	LCD_Dir =0xFF;
	_delay_ms(20);
//	cmdpin&=~(1<<rs);                           // rs = 0
//	cmdpin&=~(1<<en);                           // enable pin =0
	send_cmd(0x02);        // send 33,32 , init , displayon,cursoron
	
	
	send_cmd(0x28);
	send_cmd(0x0C);
	send_cmd(0x06);
	

	send_cmd(0x01);        // clear lcd
	_delay_ms(2);      //  wait 2 milliseconds
	
}
void lcd_Srint(char * str)
{
	
	int i;
	for (i=0;str[i]!=0;i++)
	{
		send_data(str[i]);
	}
	
	
	
	
	
	//unsigned char i=0;
	//while(str[i]!=0)
	//{
	//	send_data(str[i]);
	//	_delay_ms(200);
	//	i++;
	//}
}
void set_cursor(unsigned char x,unsigned char y,char * str)
{   
	if (x == 0 && y < 16 )
	send_cmd((y & 0x0F)|0x80);
//	unsigned char commands [] ={0x80,0xC0};
	else if(x==1 && y <16)
	send_cmd((y & 0x0F)|0xC0);
//	_delay_us(100);
   lcd_Srint(str);
}
void lcd_clear()
{
	send_cmd(0x01);
	_delay_ms(2);
	send_cmd(0x80);
}

int main(void)
{
	lcd_intil();
	lcd_Srint("theworldis");
	send_cmd(0xC0);
	lcd_Srint("theworldis");
	//lcd_clear();
	//lcd_print('A');
	//set_cursor(1,2);
	//lcd_print('a');
	//_delay_ms(1000);
	while (1)
	{  //send_data(0x41);
		//set_cursor(1,1);
		//lcd_print("the world is");
		//set_cursor(1,2);
		//lcd_print("beautiful");
		//_delay_ms(1000);
	}
}
