//The starting point for this is Version 0 of the Hastings Fiberglass Voltmeter.
//Version Version_Major.Version_Minor
//Software Version Number (0 to 9 only)
#define Version_Major 8
#define Version_Minor 9

/*
August 24, 2012 - Rev 8.3
Modifying the way that the buttons operate to more close mimic the Loadspotter.
The on button will only be used to turn the unit ON and OFF and to turn on the backlight.
The menu button will only be used to change between modes of operation.

This is the description from Lewis in email dated Aug 24, 2012

The Power button should turn on instantly as it does now.  It appears that
you currently have to hold it for four seconds to shut off, so we need the
light toggle to happen after holding it for two seconds.

The Mode button needs to respond instantly when pressed and switch from AC
to HIPOT to DC and then repeat with each push of the button.  The display
can be the same as the existing menu display but we want the selected
function to be implemented two seconds after the last push of the button.
If the unit is in AC and you push the button, it should show the arrow on
the HIPOT position and if no additional buttons are pushed, it should switch
to the HIPOT function.  If the unit is in AC and you push the button once
(showing the arrow at HIPOT) and then press it again within two seconds, the
arrow should move to DC.  Each additional push of the button within two
seconds of the last contact should repeat the scroll through the menu.  If
no button is pushed for two seconds, the last mode selected should be
implemented.

October 10, 2012 - Rev 8.4

We have had to change the scaling for the AC input which means that we will need to
modify the pass/fail criteria for the self test.

Dec 20, 2012 - Rev 8.6
Modify to test for DC when operating in HiPot mode.  Also investigate the issue of
the bar graph being incorrect fo Hi Pot


Dec 20, 2012 - Rev 8.7
Backing out of the changes to the bargraph display.

Dec 21, 2012 - Rev 8.8
Modify so that in Hipot mode the value displayed is the sum of the AC and DC values.
Double the range of the bar graph.
Back out of the changes associated with the mode check in Hipot.

Feb 4, 2013 - Rev 8.9
Modified the threshold levels in the range detection to increase them all some.
the latest run of modules was nearing the upper thresholds at the highest range.

/*
//******************************************************************
//	Description of pointers used in software
//
MenuPtr - Serves as indicator of which menu to display when called upon.
	0 - Display power-up menu with version
	1 - Display "Unable to detect connected resistors" menu to set range
	2 - No longer used (Originally used to turn backlight on and off)
	3 - Change operating mode (ACDCPtr value).

ModePtr - Serves as an indicator to the input range the unit is operating in
	This was originally set manually with a separate menu which is why it
	is called a pointer but was superceded by the auto range detection.
	Valid values are:
	1 -  40kV
	2 -  80kV
	3 - 120kV
	4 - 160kV
	5 - 200kV
	6 - 240kV

ACDCPtr - Serves as indicator as to whether unit is operating in AC, DC or
	HiPot mode as well as a pointer into the menu screen for locating the 
	selection indicator when changing mode.  Values are:
	2 - AC Operation
	3 - HiPot Operation
	4 - DC Operation

*/

//TO Do
/*


Look at using pointers passed to sub-routines to simplify the code


*/
//*******************************************************************
//
// Defines for conditional compiles
//
//*******************************************************************
// Display Selection
// uncomment this line for OPTREX Display Parameters
#define optrex 1
// otherwise uncomment this line for the New Haven Display
//#undef optrex

// IO Configuration (Board revision level)
// uncomment this line for NEW_IO Controls
#define new_io 1
// otherwise uncomment this line for the old IO controls
//#undef new_io


#ifdef optrex
//===================================================================
// OPTREX PARAMETERS
//===================================================================
#define LOW_COLUMN 		0
#define HIGH_COLUMN 	132
#define SCREEN_WIDTH 	132
#define SCREEN_HEIGHT	64
#define PIXELS_PER_PAGE	8
#define SCREEN_EVR		0x20
//#define SCREEN_EVR		0x24

#else
//===================================================================
// NEW HAVEN PARAMETERS
//===================================================================
#define LOW_COLUMN 4
#define HIGH_COLUMN 136
#define SCREEN_WIDTH 	132
#define SCREEN_HEIGHT	72
#define PIXELS_PER_PAGE	8
#define SCREEN_EVR		0x14

#endif


#ifdef new_io
//===================================================================
// IO Controls for new Combined board
//===================================================================
#define Pushbutton bit_test(portb,0)
#define Selectbutton bit_test(portf,5)
#define EnableA2D output_low(PIN_E4)
#define DisableA2D output_high(PIN_E4)
#define PowerOn bit_set(portb,1)
#define PowerOff bit_clear(portb,1)
#define BacklightOn bit_set(portb,2)
#define BacklightOff bit_clear(portb,2)
#define SPIDataSet bit_set(porte,1)
#define SPIDataClear bit_clear(porte,1)
#define SPIClkSet output_high(PIN_E0)
#define SPIClkClear output_low(PIN_E0)
#define SPIDataTest bit_test(porte,2)
#define PickUpTestRelay bit_clear(porte,7)
#define DropOutTestRelay bit_set(porte,7)
#define ApplyTestSignal bit_set(portf, 4)
#define RemoveTestSignal bit_clear(portf, 4)
#define BacklightTest bit_test(portb,2)

#define _TRISA 0b11111111
#define _TRISB 0b11111001
#define _TRISC 0b00000000
#define _TRISD 0b11111111
#define _TRISE 0b00111111
#define _TRISF 0b11101111
#define _TRISG 0b11111000
#define _Port_B_Pullups 0

#else
//===================================================================
// IO Controls for the older display boards prototype
//===================================================================
#define Pushbutton bit_test(portb,5)
#define EnableA2D bit_clear(portb,3)
#define DisableA2D bit_set(portb,3)
#define PowerOn bit_set(portf,6)
#define PowerOff bit_clear(portf,6)
#define BacklightOn bit_set(portf,7)
#define BacklightOff bit_clear(portf,7)
#define SPIDataSet bit_set(portb,1)
#define SPIDataClear bit_clear(portb,1)
#define SPIClkSet bit_set(portb,0)
#define SPIClkClear bit_clear(portb,0)
#define SPIDataTest bit_test(portb,2)
#define PickUpTestRelay bit_clear(porte,7)
#define DropOutTestRelay bit_set(porte,7)
//#define ApplyTestSignal bit_set(portf, 4)
//#define RemoveTestSignal bit_clear(portf, 4)
#define ApplyTestSignal output_high(PIN_E6)
#define RemoveTestSignal output_low(PIN_E6)
#define BacklightTest bit_test(portf,7)

output_high(PIN_E6)

#define _TRISA 0b11111111
#define _TRISB 0b11110100
#define _TRISC 0b00000000
#define _TRISD 0b11111111
#define _TRISE 0b00001100
#define _TRISF 0b11101111
#define _TRISG 0b11111000
#define _Port_B_Pullups 1

#endif

#define CHAR_PER_LINE	18
#define MAX_LINES		8






// uncomment this line for debugging
//#define debug 1
#define A2D_OFFSET 0
#define PUSHBUTTON_POWER_OFF_TIME 20	// number of cycles button must be pushed to turn off power
#define PUSHBUTTON_OFF_TIME 10			// number of cycles button must be released to be determined off
#define BACKLIGHT_ON_TIME 30			// number of 100 ms cycles button must be pushed at start up to turn on backlight
#define BACKLIGHT_OFF_TIME 5*60*10		// number of 100 ms cycles unit backlight turns off after last button push
#define BACKLIGHT_TOGGLE_TIME 11		// number of cycles before toggling backlight
#define POWER_OFF_TIME 25*60*10			// number of 100 ms cycles unit powers off after last button push
#define DISPLAY_TEST_TIME 20			// number of 100 ms cycles display shows 1888 on power up
#define DISPLAY_ZERO_TIME 20			// display shows zero

//DC Polarity Defines
#define Positive 12						// index to use to display Plus symbol
#define Negative 11						// index to use to display Minus symbol

#include "16f946.h"
#include "p16f946_inc.h"
#include "font5x7low.h"
#include "font5x7high.h"
#include "Numbers30x40.h"
#include "Menus.h"

static const int16 DCOffset = 0;

#fuses noWDT,noPUT,noPROTECT,MCLR,NOCPD,NOBROWNOUT,NOIESO,NOFCMEN,NODEBUG,INTRC_IO
#device ADC=10

//===================================================================

// with clock at 4 MHz...
// timer 1 increments at 125000 Hz, 1 count = 8 us
#use delay(clock=4000000)


//Display Control Lines
//CS1 portc,0  0=Enables , 1 = Disabled
#define DispCS1Set output_high(PIN_C0)
#define DispCS1Clear output_low(PIN_C0)
//Reset portc,1 0=Reset , 1=Run
#define DispResetSet output_high(PIN_C1)
#define DispResetClear output_low(PIN_C1)
//A0 portc,2  0=Command , 1 = Data
#define DispA0Set output_high(PIN_C2)
#define DispA0Clear output_low(PIN_C2)
//RnW portc,3 0=Write , 1=Read
#define DispRnWSet output_high(PIN_C3)
#define DispRnWClear output_low(PIN_C3)
//E portc,4 Falling Edge Trigger
#define DispESet output_high(PIN_C4)
#define DispEClear output_low(PIN_C4)
//C86 portg,1 0=8080 Interface , 0 = 6800 Interface
#define DispC86Set output_high(PIN_G1)
#define DispC86Clear output_low(PIN_G1)
//PS portg,2  0=Serial Interface , 1=Parallel Interface
#define DispPSSet output_high(PIN_G2)
#define DispPSClear output_low(PIN_G2)

#define FONT_OFFSET 32
#define FONT_WIDTH	5
#define LRG_FONT_WIDTH 30
#define PASS TRUE
#define FAIL FALSE

//define self test limits
#define SelfTestLowerLimit 1150
#define SelfTestUpperLimit 1530


// TIMERS
int1 PushButtonOld;
int16 PushButtonOnCounter, PushButtonOffCounter;
int PushButtonCycleCounter;
int16 PowerOffCounter;
int16 PowerUpCounter;

char test;
char index;
unsigned char Range;
int16 temp;
int TestResult;

int32 CurrentReading, NewCurrentReading;
int16 DisplayData;
int16 DCInput;
int16 peak;

int32 bin;
#locate bin	=			0x3c
#locate bin1  =			0x3d
#locate bin2	=		0x3e
#locate bin3	=		0x3f

int32 abcd;
#locate abcd	= 		0x40
#locate abcd1	= 		0x41
#locate abcd2	= 		0x42
#locate abcd3	= 		0x43

#locate ii	=			0x44
#locate cnt	=			0x45

#locate temp8 = 		0x46



long int i, DisplayCounter;
signed long int stemp16;
int16 BatteryVoltage;
int a2d_h,a2d_m,a2d_l,CycleCounter;
int sentinel;
int DisplayUpdateRate;
int MenuPtr, ModePtr, ACDCPtr, ModeErr, PtrSave, RangeSave;
unsigned int DisplayLoBatt;
unsigned int DCPolarity;


void b2bcd(void);
void clear_lcd(void);
void UpdateDisplay(int32);
void GLCD_Initialize(void);
void GLCD_WriteCommand(unsigned char);
void GLCD_WriteData(unsigned char);
unsigned char GLCD_ReadStatus(void);
void GLCD_GoTo(unsigned char , unsigned char );
void GLCD_ClearScreen(void);
void GLCD_WriteChar(char);
void GLCD_WriteBarGraph(char);
void GLCD_WriteLargeChar(char , unsigned char);
void GLCD_WriteDecimal(unsigned char);
void Display_Menu(unsigned char);
void Update_Menu_Ptr(unsigned char);
void Update_Mode_Disp(unsigned char);
void Update_Peak_Ptr(int16);
void BatteryDisplay(int32, unsigned char);
void ReadExtAtoD(void);
int16 SampleADC(int, int);
void CheckMode(void);
void RangeCheck(void);
void CalCheck(int);
void SetRange(void);

void main() {

//==========================================================
//		Initialize on Power Up
//==========================================================

	port_b_pullups(_Port_B_Pullups);
	trisa = _TRISA;
	trisb = _TRISB;
	trisc = _TRISC;
	trisd = _TRISD;
	trise = _TRISE;
	trisf = _TRISF;
	trisg = _TRISG;

	PowerOff;
	BacklightOff;

// Initialize test port outputs
	DropOutTestRelay;
	RemoveTestSignal;


	delay_ms(250);

	PowerOn;



//============
// setup ADC
//============
	setup_adc(ADC_CLOCK_DIV_32);
	setup_adc_ports(sAN0|sAN1|sAN2);
	set_adc_channel(0);
	bit_clear(ANSEL,0);
	bit_clear( ADCON0,ADFM );

	
//============
// setup LCD 
//============
	LCDCON = 0b00000000;	// disabled

//============
// setup oscillator
//============
//use only for internal oscillator 
	setup_oscillator(OSC_4MHZ);		// use internal 4 MHz clock

	PushButtonOnCounter = 0;
	PushButtonOffCounter = 0;
	PowerUpCounter = 0;
	CycleCounter = 0;
	NewCurrentReading = 0;
	DisplayUpdateRate = 5;
	DisplayLoBatt = TRUE;

	DisplayCounter = 0;

	// power up, if pushbutton is press longer than 3 seconds, light up backlight
	PowerUpCounter = 0;

//	Initialize the display
//
	GLCD_Initialize();

	delay_ms(100);
	
	GLCD_ClearScreen();
	Display_Menu(0);

	GLCD_GoTo(110,2);
	GLCD_WriteChar(Version_Major + 0x30);
	GLCD_WriteChar('.');
	GLCD_WriteChar(Version_Minor + 0x30);



	do{
		delay_ms(100);
		PowerUpCounter++;
		if( PowerUpCounter > BACKLIGHT_ON_TIME )
		{
			BacklightOn;
		}
	} while ( PushButton );

//	// power up done, now run

	test = 0;
	peak = 0;
	Range = 5;
	MenuPtr = 0;
	ModePtr = 1;
	ACDCPtr = 2;
	DCPolarity = Positive;
	ModeErr = FALSE;


//==========================================================
//		battery voltage check
//
// Tests indicate that the sum of 10 samples of the battery
// input is approximately:
// Battery Voltage   Sample Sum
//        3.2            6560
//        3.1            6360
//        3.0            6160
//        2.9            5960
//        2.8            5720
//        2.7            5560
//        2.6            5320
//        2.5            5120
//        2.4            4880
//==========================================================

	BatteryVoltage = SampleADC(0, 10);
	
	GLCD_GoTo(110,3);
	if(BatteryVoltage < 5720)
	{
		GLCD_WriteChar('L');
		GLCD_WriteChar('O');
		GLCD_WriteChar('W');
	}
	else
	{
		GLCD_WriteChar('O');
		GLCD_WriteChar('K');
		GLCD_WriteChar(' ');
	}

	GLCD_GoTo(80,5);
	GLCD_WriteChar('T');
	GLCD_WriteChar('e');
	GLCD_WriteChar('s');
	GLCD_WriteChar('t');
	GLCD_WriteChar('i');
	GLCD_WriteChar('n');
	GLCD_WriteChar('g');


	DisableA2D;


//----------------------------------------------------------------------------
	PickUpTestRelay;

	for(test=0 ; test<2 ; test++)
	{
		//This is where we run the self test
		for(stemp16=0 ; stemp16<60 ; stemp16++)
		{
			output_high(PIN_E6);
			delay_us(8333);
			output_low(PIN_E6);
			delay_us(8333);
		}
	
			EnableA2D;
	
		for(stemp16=0 ; stemp16<6 ; stemp16++)
		{
			output_high(PIN_E6);
			delay_us(8333);
			output_low(PIN_E6);
			delay_us(8333);
		}
	
		TestResult = Fail;

		ReadExtAtoD();

		delay_ms(100);
	}

	DropOutTestRelay;

//----------------------------------------------------------------------------

	if(TestResult == Pass)
	{
		GLCD_GoTo(80,5);
		GLCD_WriteChar('P');
		GLCD_WriteChar('a');
		GLCD_WriteChar('s');
		GLCD_WriteChar('s');
		GLCD_WriteChar('e');
		GLCD_WriteChar('d');
		GLCD_WriteChar(' ');
	}
	else
	{
//ddd Mar 9 2012 Temporarily enable display
		BatteryDisplay((int32)NewCurrentReading, 1);
		temp = 10;
		do{
			GLCD_GoTo(80,5);
			GLCD_WriteChar('F');
			GLCD_WriteChar('a');
			GLCD_WriteChar('i');
			GLCD_WriteChar('l');
			GLCD_WriteChar('e');
			GLCD_WriteChar('d');
			GLCD_WriteChar(' ');
			delay_ms(500);
			GLCD_GoTo(80,5);
			GLCD_WriteChar(' ');
			GLCD_WriteChar(' ');
			GLCD_WriteChar(' ');
			GLCD_WriteChar(' ');
			GLCD_WriteChar(' ');
			GLCD_WriteChar(' ');
			GLCD_WriteChar(' ');
			delay_ms(500);
			temp--;
		} while ( temp != 0 );
	}

//-----------------------------------------------------------------------------
// Try to determine number of resistors installed


	SetRange();

//-----------------------------------------------------------------------------

	GLCD_ClearScreen();
	if(ModePtr == 7)
	{
		ModePtr = 1;
	}

	RangeSave = ModePtr;  //Save for future reference

DisplayMenu:

	if(MenuPtr != 0)
	{

		GLCD_ClearScreen();
		Display_Menu(3);
	
		Update_Menu_Ptr(ACDCPtr);
		PtrSave = ACDCPtr;
		do{
			//Wait for menu button to be released
		} while (!SelectButton);
		delay_ms(100);

		temp = 100;
		do{
			if(!SelectButton)
			{
				ACDCPtr++;
				if(ACDCPtr > 4)
				{
					ACDCPtr = 2;
				}
				Update_Menu_Ptr(ACDCPtr);
				delay_ms(100);
				do{
					//Reset for 2 seconds
					temp = 20; 
					//Will need some break out of here
				} while (!SelectButton);
				delay_ms(100);
			}
			//Will need a breakout of here as well			
			delay_ms(100);
			temp--;
	
		} while (temp != 0);

		do{
			//Wait for menu button to be released
		} while (!SelectButton);


		delay_ms(100);
		if (ACDCPtr != PtrSave)
		{
			if (ACDCPtr == 3)
			{
				ModePtr = 7;
				peak = 0;				//ddd Debug Dec 21
			}
			else if (ACDCPtr == 2)
			{
//				ModePtr = 1;
				ModePtr = RangeSave;
//ddd debug				SetRange();
//				if(ModePtr > 6)
//				{
//					ModePtr = 1;
//				}
			}
			else
			{
				if(ACDCPtr == 4)
				{
					ModePtr = 1;
				}
			}
		}			
	}

	GLCD_ClearScreen();

	Update_Mode_Disp(ModePtr-1);

	Range = ModePtr-1;

// Initialize the screen
	GLCD_WriteBarGraph(0);
	GLCD_WriteLargeChar(11, 0);
	GLCD_WriteLargeChar(11, 30);
	GLCD_WriteLargeChar(11, 60);
	GLCD_WriteDecimal(90);
	GLCD_WriteLargeChar(11, 94);

	// power up done, now run
	PushButtonCycleCounter = 0;
	PowerOffCounter = 0;


	while (1)
	{
	// cycle time is 100ms of which approximately 75ms is the A/D conversion time
	// and 25ms is an intentional delay.
		PowerOffCounter++;
		if( !PowerOffCounter )PowerOffCounter = 0xffff;		// counts up to 0xffff
		PowerUpCounter++;
		if( !PowerUpCounter )PowerUpCounter = 0xffff;		// counts up to 0xffff

//==========================================================
//		Pushbutton Handler
//==========================================================
		// 

		if( PushButton ){
			// when mode changes, increment pushbutton counter
			if( PushButtonOld == 0 )
			{
				PushButtonCycleCounter++;
				PowerOffCounter = 0;
			}
			// push button was already down, if longer than POWER_OFF_TIME turn off
			// by setting PowerOffCounter to limit value
			else
			{
				PushButtonOnCounter++;
				if( PushButtonOnCounter > PUSHBUTTON_POWER_OFF_TIME )
				{
					PowerOffCounter = POWER_OFF_TIME + 1;
				}
				else
				{
					if(PushButtonOnCounter == BACKLIGHT_TOGGLE_TIME)
					{
						if(BacklightTest)		//Test current status of Backlight
						{
							BacklightOff;
						}
						else
						{
							BacklightOn;
						}
					}
				}
			}
			PushButtonOld = 1;
			PushButtonOffCounter = 0;
		}
		else
		{
			if( ++PushButtonOffCounter > PUSHBUTTON_OFF_TIME ){
//				if( PushButtonCycleCounter == 2 ) BacklightOn;
//				else if( PushButtonCycleCounter == 4 ) BacklightOff;
				PushButtonCycleCounter = 0;
			}
			PushButtonOld = 0;
			PushButtonOnCounter = 0;
		}
		if( PowerOffCounter > BACKLIGHT_OFF_TIME )
		{
			BacklightOff;
		}

		if( PowerOffCounter > POWER_OFF_TIME )
		{
			GLCD_ClearScreen();
			PowerOff;
			while(1);
		}

//==========================================================
//		a/d handler
//==========================================================
		// get a2d via SPI

		delay_ms(25);
		EnableA2D;
		ReadExtAtoD();


//==========================================================
//		battery voltage check
//==========================================================

		BatteryVoltage = SampleADC(0, 10);

//==========================================================
//		DC Input voltage check
//==========================================================
		DCInput=SampleADC(2, 32);
		DCInput = DCInput/32;
		DCInput = DCInput + DCOffset;
		if(DCInput > 512)
		{
			DCPolarity = Negative;
			DCInput = DCInput - 512;
		}
		else
		{
			DCPolarity = Positive;
			DCInput = 512 - DCInput;
		}
		set_adc_channel(0);  //Set ADC to read battery voltage input channel
//==========================================================
//		process reading
//==========================================================
		// filter

		CurrentReading = NewCurrentReading * 2 / 5;

//
//==========================================================
//		Check for a mode error 
//==========================================================
//********************************************************
		CheckMode();
//************************************************************
//==========================================================
//		update displayed parameter
//==========================================================
// ddd Debug Dec 21
		if(ACDCPtr == 2)
		{
			DisplayData = CurrentReading;
		}
		else if(ACDCPtr == 3)
		{
			DisplayData = CurrentReading + (DCInput/10);
		}
		else
		{
			DisplayData = DCInput;
		}
//ddd End Debug Dec 21		

		if( DisplayData > 440 ) DisplayData = 440;    //????????


		CycleCounter++;
		if(CycleCounter > DisplayUpdateRate)
		{					// update display at display update rate
			CycleCounter = 0;
//			DisplayLoBatt is used to flash the Low Battery Indication 
			if(DisplayLoBatt == TRUE)
				DisplayLoBatt = FALSE;
			else
				DisplayLoBatt = TRUE;

			if(!Selectbutton)
			{
				MenuPtr = 3;
				goto DisplayMenu;
			}
			else
			{
				if(ModePtr <7)
				{
					if(ModeErr == TRUE)
						UpdateDisplay(9999);
					else if(DisplayData == 440)
						UpdateDisplay(9999);
					else
					{
						if(ACDCPtr == 2)
							UpdateDisplay(DisplayData * ModePtr);
						else
							UpdateDisplay(DisplayData);
					}
				}
				else
				{
					//This is HiPot?
					if(DisplayData == 440)
						UpdateDisplay(9999);
					else
						UpdateDisplay(DisplayData);
				}
			}

//Check for low battery and update display as appropriate
			GLCD_GoTo(110,7);
			if(ModePtr <7)
			{
				switch( ACDCPtr )
				{
					case 2: //If it is a 2
						GLCD_WriteChar(' ');
						GLCD_WriteChar('A');
						GLCD_WriteChar('C');
					break;
					case 4: //If it is a 4
						GLCD_WriteChar(' ');
						GLCD_WriteChar('D');
						GLCD_WriteChar('C');
					break;
			
					default:
						GLCD_WriteChar(' ');
						GLCD_WriteChar(' ');
						GLCD_WriteChar(' ');
					break;
				}

			}
			else
			{
				GLCD_WriteChar('H');
				GLCD_WriteChar('P');
				GLCD_WriteChar('t');
			}
			
			if( BatteryVoltage < 5320 )
			{
				if(DisplayLoBatt == TRUE)
				{
					GLCD_GoTo(110,7);
					GLCD_WriteChar('B');
					GLCD_WriteChar('a');
					GLCD_WriteChar('t');
				}
			}
		}

		if((DisplayData/4) == 0)
		{
			peak = 0;
		}
		else if(DisplayData>peak)
		{
			peak = DisplayData;
		}
		
		if((Range == 6)&&(peak!=0))
		{
			if((DisplayData/2) < 100)				
			{
				GLCD_WriteBarGraph(DisplayData/2);
			}
			else
			{
				GLCD_WriteBarGraph(100);
			}
			if(peak > 6)
			{
				Update_Peak_Ptr(peak);
			}
		}
		else
		{
			GLCD_WriteBarGraph(DisplayData/4);
		}
	}
}

//===================================================================================

void UpdateDisplay(int32 DisplayData)
{
	unsigned int blank;

	if(DisplayData != 9999)
	{
		bin = DisplayData;
		abcd = 0;
		b2bcd();
		b2bcd();
		b2bcd();
		b2bcd();
		b2bcd();
		b2bcd();
		b2bcd();
		b2bcd();
	// This is where we need to write to the display
	
		blank = TRUE;
		if(ACDCPtr != 4)
		{
			index = make8(abcd,1) >> 4;
			if(index == 0 && blank == TRUE)
			{
				index = 10;
			}
			else
			{
				blank = FALSE;
			}
		}
		else
		{
			index = DCPolarity;
			blank = FALSE;
		}
		GLCD_WriteLargeChar(index, 0);
		index = make8(abcd,1) & 0x0f;
		if(index == 0 && blank == TRUE)
		{
			index = 10;
		}
		else
		{
			blank = FALSE;
		}	
		GLCD_WriteLargeChar(index, 30);
		index = make8(abcd,0) >> 4;
		if(ACDCPtr ==4)
		{
			GLCD_WriteDecimal(60);
			GLCD_WriteLargeChar(index, 64);
		}
		else
		{
			GLCD_WriteLargeChar(index, 60);
			GLCD_WriteDecimal(90);
		}
		index = make8(abcd,0) & 0x0f;
		GLCD_WriteLargeChar(index, 94);
	
		GLCD_GoTo(110,5);
		GLCD_WriteChar(' ');
		GLCD_WriteChar('k');
		GLCD_WriteChar('V');
	}
	else
	{
		if(DisplayLoBatt == TRUE)
		{
			if(ModeErr == FALSE)
				index = 12;
			else
				index = 13;		//Display ERRSYM
		}
		else
			index = 10;
		GLCD_WriteLargeChar(index, 0);
		GLCD_WriteLargeChar(index, 30);
		if(ACDCPtr ==4)
		{
			GLCD_WriteDecimal(60);
			GLCD_WriteLargeChar(index, 64);
		}
		else
		{
			GLCD_WriteLargeChar(index, 60);
			GLCD_WriteDecimal(90);
		}
		GLCD_WriteLargeChar(index, 94);
	}
}

//=================================================================
// 16-bit binary to 4-digit bcd converter
//=================================================================
void b2bcd(){

// takes about 114 us (8 bits)
	#asm

	movlw	4		// 32-bits
	movwf	ii		// make cycle counter
	
b2bcd2:	movlw	abcd		// make pointer
	movwf	fsr
	movlw	4
	movwf	cnt

b2bcd3:	movlw	0x33		
	addwf	indf,f		// add to both nibbles
	btfsc	indf,3		// test if low result � 7
	andlw	0xf0		// low result so take the 3 out
	btfsc	indf,7		// test if high result � 7
	andlw	0x0f		// high result � 7 so ok
	subwf	indf,f		// any results �= 7, subtract back
	incf	fsr,f		// point to next
	decfsz	cnt
	goto	b2bcd3
	
	rlf	&bin,f		// get another bit
	rlf	&(int)bin+1,f
	rlf	&(int)bin+2,f
	rlf	&(int)bin+3,f
	rlf	&abcd,f		// put it into bcd
	rlf	&(int)abcd+1,f
	rlf	&(int)abcd+2,f
	rlf	&(int)abcd+3,f

	decfsz	ii,f		// all done?
	goto	b2bcd2		// no, loop
	#endasm
}

void error(int number)
{
}
//=========================================================================================
void BatteryDisplay(int32 DisplayData, unsigned int mode)
{
	bin = DisplayData;
	abcd = 0;
	b2bcd();
	b2bcd();
	b2bcd();
	b2bcd();
	b2bcd();
	b2bcd();
	b2bcd();
	b2bcd();
// This is where we need to write to the display

	GLCD_GoTo(110,3);
	if(mode)
	{
		GLCD_GoTo(90,6);
		index = make8(abcd,1) >> 4;
		GLCD_WriteChar(index + 0x30);
		index = make8(abcd,1) & 0x0f;
		GLCD_WriteChar(index + 0x30);
	}
	index = make8(abcd,0) >> 4;
	GLCD_WriteChar(index + 0x30);
	GLCD_WriteChar('.');
	index = make8(abcd,0) & 0x0f;
	GLCD_WriteChar(index + 0x30);
	GLCD_WriteChar('V');

	delay_ms(2000);

}

//-------------------------------------------------------------------------------------------------
//
//-------------------------------------------------------------------------------------------------
void GLCD_WriteCommand(unsigned char commandcode)
{
	//Wait for not busy
//	while(GLCD_ReadStatus() & 0x90);
	//
	DispA0Clear;			//select command mode
	DispRnWClear;			//select write mode
	DispCS1Clear;			//enable the display interface
	OUTPUT_D(commandcode);	//output data on the port
	delay_us(2);
	DispESet;				//raise the enable signal to start the data transfer
	delay_us(2);
	DispEClear;				//clock the data 
	DispCS1Set;				//disable the display interface
}
//-------------------------------------------------------------------------------------------------
//
//-------------------------------------------------------------------------------------------------
void GLCD_WriteData(unsigned char datachar)
{
	//Wait for not busy
	//while(GLCD_ReadStatus() & 0x90);
	//
	DispA0Set;				//select data mode
	DispRnWClear;			//select write mode
	DispCS1Clear;			//enable the display interface
	OUTPUT_D(datachar);		//output data on the port
	delay_us(2);
	DispESet;				//raise the enable signal to start the data transfer
	delay_us(2);
	DispEClear;				//clock the data 
	DispCS1Set;				//disable the display interface
}
//-------------------------------------------------------------------------------------------------
//
//-------------------------------------------------------------------------------------------------
void GLCD_Initialize(void)
{
	//C86 portg,1 1=6800 Interface , 0 = 8080 Interface
	output_high(PIN_G1);		
	//PS portg,2  0=Serial Interface , 1=Parallel Interface
	output_high(PIN_G2);
	DispESet;							//Ensure E is high
	DispRnWSet;							//Set interface in Read mode
	DispCS1Clear;						//Enable the interface
	DispResetSet;						//Ensure Reset line is high
	DispResetClear;						//Pull the Reset line low
	delay_ms(2);						//Apply Reset signal for minimum 2 ms
	DispResetSet;						//Remove the Reset signal
	delay_ms(100);						//Wait for interface internal reset
	GLCD_WriteCommand(0xE2);			//Internal Reset		
	GLCD_WriteCommand(0xA2);			//Bias 1/9		
	GLCD_WriteCommand(0xA1);			//ADC Select, Reverse		
	GLCD_WriteCommand(0xC0);			//Com Output, Normal		
	GLCD_WriteCommand(0xA4);			//Display All Points, Normal Display		
	GLCD_WriteCommand(0x40);			//Display Start Line Set, 0
	GLCD_WriteCommand(0x25);			//Internal Resistor Ratio Set, 5

	GLCD_WriteCommand(0x81);			//Electronic Volume Mode Set Register		
	GLCD_WriteCommand(SCREEN_EVR);		//Electronic Volume Set to 0x12	for New Haven Display	

	GLCD_WriteCommand(0x2F);			//Select Internal Power Supply Operating Mode		
	delay_ms(100);						//Wait for interface internal reset
	GLCD_WriteCommand(0xAF);			//Display ON		
	delay_ms(100);						//Wait for interface internal reset
}


//-------------------------------------------------------------------------------------------------
//
//-------------------------------------------------------------------------------------------------
void GLCD_GoTo(unsigned char x, unsigned char y)
{
	GLCD_WriteCommand(0xB0 | y);
	GLCD_WriteCommand(0x10 | (x >> 4));
	GLCD_WriteCommand(0x00 | (x & 0x0F));
}
//-------------------------------------------------------------------------------------------------
//
//-------------------------------------------------------------------------------------------------
void GLCD_ClearScreen(void)
{
	unsigned char x = 0, y = 0;
	for(y = 0; y < (SCREEN_HEIGHT/PIXELS_PER_PAGE); y++)
	{
		GLCD_GoTo(0,y);
		for(x = 0; x < SCREEN_WIDTH; x++)
		{
			GLCD_WriteData(0);
		}
	}
}		
//-------------------------------------------------------------------------------------------------
// Function : GLCD_WriteChar
// Artuments : Char ASCII code
// Return value : none
//-------------------------------------------------------------------------------------------------
void GLCD_WriteChar(char charCode)
{
	unsigned char fontCollumn;
	int16 location;

	if(charCode < 82)
	{
		for(fontCollumn = 0; fontCollumn < FONT_WIDTH; fontCollumn++)
		{
			location = charCode;
			location = ((location - FONT_OFFSET) * FONT_WIDTH) + fontCollumn;
			GLCD_WriteData(font5x7low[location]);
		}
	}
	else
	{
		for(fontCollumn = 0; fontCollumn < FONT_WIDTH; fontCollumn++)
		{
			location = charCode-50;
			location = ((location - FONT_OFFSET) * FONT_WIDTH) + fontCollumn;
			GLCD_WriteData(font5x7high[location]);
		}
	}
	GLCD_WriteData(0);
}
//-------------------------------------------------------------------------------------------------
// Function : GLCD_WriteDecimal
// Artuments : none
// Return value : none
//-------------------------------------------------------------------------------------------------
void GLCD_WriteDecimal(unsigned char offset)
{
	unsigned char fontCollumn;
	for(temp=0 ; temp<5 ; temp++)
	{
		GLCD_GoTo(LOW_COLUMN+offset,(temp));
		for(fontCollumn = 0; fontCollumn < 4; fontCollumn++)
		{
			GLCD_WriteData(0x00);
		}
	}
	GLCD_GoTo(LOW_COLUMN+offset,4);
	GLCD_WriteData(0x0F);
	GLCD_WriteData(0x0F);
	GLCD_WriteData(0x0F);
	GLCD_WriteData(0x0F);
}

//-------------------------------------------------------------------------------------------------
// Function : GLCD_BarGraph
// Artuments : Char level
// Return value : none
//-------------------------------------------------------------------------------------------------
void GLCD_WriteBarGraph(char level)
{
	unsigned char fontCollumn;
	if(level > 100)
	{
		level = 100;
	}
	GLCD_GoTo(LOW_COLUMN+6,5);

	GLCD_WriteData(0xF0);
	for(fontCollumn = 0; fontCollumn < level; fontCollumn++)
	{
		GLCD_WriteData(0xF0);
	}
	for(fontCollumn = level; fontCollumn < 100; fontCollumn++)
	{
		if((fontCollumn == 25)||(fontCollumn == 50)||(fontCollumn == 75))
			GLCD_WriteData(0xF0);
		else
			GLCD_WriteData(0x10);
	}
	GLCD_WriteData(0xF0);

	GLCD_GoTo(LOW_COLUMN,6);

	GLCD_WriteChar('0');

	GLCD_WriteData(0xFF);
	for(fontCollumn = 0; fontCollumn < level; fontCollumn++)
	{
		GLCD_WriteData(0xFF);
	}
	for(fontCollumn = level; fontCollumn < 100; fontCollumn++)
	{
		GLCD_WriteData(0x00);
	}
	GLCD_WriteData(0xFF);

	GLCD_GoTo(LOW_COLUMN+6,7);

	GLCD_WriteData(0xFF);
	for(fontCollumn = 0; fontCollumn < level; fontCollumn++)
	{
		GLCD_WriteData(0xFF);
	}
	for(fontCollumn = level; fontCollumn < 100; fontCollumn++)
	{
		if((fontCollumn == 25)||(fontCollumn == 50)||(fontCollumn == 75))
			GLCD_WriteData(0xF0);
		else
			GLCD_WriteData(0x80);
	}
	GLCD_WriteData(0xFF);
}


//-------------------------------------------------------------------------------------------------
// Function : GLCD_WriteChar
// Artuments : Char ASCII code
// Return value : none
//-------------------------------------------------------------------------------------------------
void GLCD_WriteLargeChar(char charCode, unsigned char offset)
{
	unsigned char fontCollumn;

	switch( charCode )
	{
		case 0: //If it is a zero
			for(temp=0 ; temp<5 ; temp++)
			{
				GLCD_GoTo(LOW_COLUMN+offset,(temp));
				for(fontCollumn = 0; fontCollumn < LRG_FONT_WIDTH; fontCollumn++)
				{
					GLCD_WriteData(zero[(temp*LRG_FONT_WIDTH)+fontCollumn]);
				}
			}
		break;
		case 1: //If it is a zero
			for(temp=0 ; temp<5 ; temp++)
			{
				GLCD_GoTo(LOW_COLUMN+offset,(temp));
				for(fontCollumn = 0; fontCollumn < LRG_FONT_WIDTH; fontCollumn++)
				{
					GLCD_WriteData(one[(temp*LRG_FONT_WIDTH)+fontCollumn]);
				}
			}
		break;
		case 2: //If it is a zero
			for(temp=0 ; temp<5 ; temp++)
			{
				GLCD_GoTo(LOW_COLUMN+offset,(temp));
				for(fontCollumn = 0; fontCollumn < LRG_FONT_WIDTH; fontCollumn++)
				{
					GLCD_WriteData(two[(temp*LRG_FONT_WIDTH)+fontCollumn]);
				}
			}
		break;
		case 3: //If it is a zero
			for(temp=0 ; temp<5 ; temp++)
			{
				GLCD_GoTo(LOW_COLUMN+offset,(temp));
				for(fontCollumn = 0; fontCollumn < LRG_FONT_WIDTH; fontCollumn++)
				{
					GLCD_WriteData(three[(temp*LRG_FONT_WIDTH)+fontCollumn]);
				}
			}
		break;
		case 4: //If it is a zero
			for(temp=0 ; temp<5 ; temp++)
			{
				GLCD_GoTo(LOW_COLUMN+offset,(temp));
				for(fontCollumn = 0; fontCollumn < LRG_FONT_WIDTH; fontCollumn++)
				{
					GLCD_WriteData(four[(temp*LRG_FONT_WIDTH)+fontCollumn]);
				}
			}
		break;
		case 5: //If it is a zero
			for(temp=0 ; temp<5 ; temp++)
			{
				GLCD_GoTo(LOW_COLUMN+offset,(temp));
				for(fontCollumn = 0; fontCollumn < LRG_FONT_WIDTH; fontCollumn++)
				{
					GLCD_WriteData(five[(temp*LRG_FONT_WIDTH)+fontCollumn]);
				}
			}
		break;
		case 6: //If it is a zero
			for(temp=0 ; temp<5 ; temp++)
			{
				GLCD_GoTo(LOW_COLUMN+offset,(temp));
				for(fontCollumn = 0; fontCollumn < LRG_FONT_WIDTH; fontCollumn++)
				{
					GLCD_WriteData(six[(temp*LRG_FONT_WIDTH)+fontCollumn]);
				}
			}
		break;
		case 7: //If it is a zero
			for(temp=0 ; temp<5 ; temp++)
			{
				GLCD_GoTo(LOW_COLUMN+offset,(temp));
				for(fontCollumn = 0; fontCollumn < LRG_FONT_WIDTH; fontCollumn++)
				{
					GLCD_WriteData(seven[(temp*LRG_FONT_WIDTH)+fontCollumn]);
				}
			}
		break;
		case 8: //If it is a zero
			for(temp=0 ; temp<5 ; temp++)
			{
				GLCD_GoTo(LOW_COLUMN+offset,(temp));
				for(fontCollumn = 0; fontCollumn < LRG_FONT_WIDTH; fontCollumn++)
				{
					GLCD_WriteData(eight[(temp*LRG_FONT_WIDTH)+fontCollumn]);
				}
			}
		break;
		case 9: //If it is a zero
			for(temp=0 ; temp<5 ; temp++)
			{
				GLCD_GoTo(LOW_COLUMN+offset,(temp));
				for(fontCollumn = 0; fontCollumn < LRG_FONT_WIDTH; fontCollumn++)
				{
					GLCD_WriteData(nine[(temp*LRG_FONT_WIDTH)+fontCollumn]);
				}
			}
		break;
		case 10: //If it is a blank
			for(temp=0 ; temp<5 ; temp++)
			{
				GLCD_GoTo(LOW_COLUMN+offset,(temp));
				for(fontCollumn = 0; fontCollumn < LRG_FONT_WIDTH; fontCollumn++)
				{
					GLCD_WriteData(blank[(temp*LRG_FONT_WIDTH)+fontCollumn]);
				}
			}
		break;

		case 11: //If it is a dash
			for(temp=0 ; temp<5 ; temp++)
			{
				GLCD_GoTo(LOW_COLUMN+offset,(temp));
				for(fontCollumn = 0; fontCollumn < LRG_FONT_WIDTH; fontCollumn++)
				{
					GLCD_WriteData(dash[(temp*LRG_FONT_WIDTH)+fontCollumn]);
				}
			}
		break;

		case 12: //If it is a plus
			for(temp=0 ; temp<5 ; temp++)
			{
				GLCD_GoTo(LOW_COLUMN+offset,(temp));
				for(fontCollumn = 0; fontCollumn < LRG_FONT_WIDTH; fontCollumn++)
				{
					GLCD_WriteData(plus[(temp*LRG_FONT_WIDTH)+fontCollumn]);
				}
			}
		break;

		case 13: //If it is an error marker
			for(temp=0 ; temp<5 ; temp++)
			{
				GLCD_GoTo(LOW_COLUMN+offset,(temp));
				for(fontCollumn = 0; fontCollumn < LRG_FONT_WIDTH; fontCollumn++)
				{
					GLCD_WriteData(errsym[(temp*LRG_FONT_WIDTH)+fontCollumn]);
				}
			}
		break;

		default:
		break;
	}
}


//-------------------------------------------------------------------------------------------------
// Function : Display
// Arguments : Unsigned Char Menu to display menu
// Return value : none
//-------------------------------------------------------------------------------------------------
void Display_Menu(unsigned char menu)
{
	int i, j;

	switch( menu )
	{
		case 0: //If it is a zero
			for(j = 0; j < 6; j++)
			{
				GLCD_GoTo(LOW_COLUMN,j);
				for(i = 0; i < CHAR_PER_LINE; i++)
				{
					GLCD_WriteChar(Menu0[(j*CHAR_PER_LINE)+i]);
				}
			}
		break;
		case 1: //If it is a 1
			for(j = 0; j < 7; j++)
			{
				GLCD_GoTo(LOW_COLUMN,j);
				for(i = 0; i < CHAR_PER_LINE; i++)
				{
					GLCD_WriteChar(Menu1[(j*CHAR_PER_LINE)+i]);
				}
			}
		break;

/*
		case 2: //If it is a 2
			for(j = 0; j < 4; j++)
			{
				GLCD_GoTo(LOW_COLUMN,j);
				for(i = 0; i < CHAR_PER_LINE; i++)
				{
					GLCD_WriteChar(Menu2[(j*CHAR_PER_LINE)+i]);
				}
			}
		break;
*/

		case 3: //If it is a 3
			for(j = 0; j < 5; j++)
			{
				GLCD_GoTo(LOW_COLUMN,j);
				for(i = 0; i < CHAR_PER_LINE; i++)
				{
					GLCD_WriteChar(Menu3[(j*CHAR_PER_LINE)+i]);
				}
			}
		break;

		default:
		break;
	}
}


//-------------------------------------------------------------------------------------------------
// Function : Display
// Arguments : Unsigned Char Menu to display menu
// Return value : none
//-------------------------------------------------------------------------------------------------
void Update_Menu_Ptr(unsigned char position)
{
	int i;
	for(i=1; i<MAX_LINES ; i++)
	{
		GLCD_GoTo(LOW_COLUMN,i);
		if(position == i)
		{
			GLCD_WriteChar(0x7F);
			GLCD_WriteChar(0x7E);
		}
		else
		{
			GLCD_WriteChar(' ');
			GLCD_WriteChar(' ');
		}
		
	}
}


//-------------------------------------------------------------------------------------------------
// Function : Display
// Arguments : Unsigned Char Menu to display menu
// Return value : none
//-------------------------------------------------------------------------------------------------
void Update_Mode_Disp(unsigned char mode)
{

	GLCD_GoTo(110,6);
	if(mode < 6)
	{
		if(ACDCPtr != 4)
		{
			switch( mode )
			{
				case 0: //If it is a zero
					GLCD_WriteChar(' ');
					GLCD_WriteChar('4');
				break;
				case 1: //If it is a 1
					GLCD_WriteChar(' ');
					GLCD_WriteChar('8');
				break;
				case 2: //If it is a 2
					GLCD_WriteChar('1');
					GLCD_WriteChar('2');
				break;
				case 3: //If it is a 3
					GLCD_WriteChar('1');
					GLCD_WriteChar('6');
				break;
				case 4: //If it is a 4
					GLCD_WriteChar('2');
					GLCD_WriteChar('0');
				break;
				case 5: //If it is a 5
					GLCD_WriteChar('2');
					GLCD_WriteChar('4');
				break;
		
				default:
				break;
			}
			GLCD_WriteChar('0');
		}
		else
		{
			GLCD_WriteChar(' ');
			GLCD_WriteChar(' ');
			GLCD_WriteChar('4');
		}			

		GLCD_GoTo(110,7);
		switch( ACDCPtr )
		{
			case 2: //If it is a 2
				GLCD_WriteChar(' ');
				GLCD_WriteChar('A');
				GLCD_WriteChar('C');
			break;
			case 4: //If it is a 4
				GLCD_WriteChar(' ');
				GLCD_WriteChar('D');
				GLCD_WriteChar('C');
			break;
	
			default:
			break;
		}
	}
	else if(mode == 6)
	{
		GLCD_WriteChar(' ');
		GLCD_WriteChar('4');
		GLCD_WriteChar('0');

		GLCD_GoTo(110,7);

		GLCD_WriteChar('H');
		GLCD_WriteChar('P');
		GLCD_WriteChar('t');
	}
	else
	{
		//We have a problem and should go to menu 1
	}
}

//-------------------------------------------------------------------------------------------------
// Function : Display
// Arguments : Unsigned Char Menu to display menu
// Return value : none
//-------------------------------------------------------------------------------------------------
void Update_Peak_Ptr(int16 peak)
{
	unsigned char position;
	int16 digit;
//

	if((peak/2) > 6)
	{
		if((peak/2) > 94)
		{
			position = 94;
		}
		else
		{
			position = (unsigned char)(peak/2);
		}
		GLCD_GoTo(LOW_COLUMN+6+position,5);
		GLCD_WriteData(0xF0);
		if(peak > 99)
		{
			GLCD_GoTo(LOW_COLUMN-6+position,6);
			digit = peak/100;
			GLCD_WriteChar(digit + 0x30);
			peak = peak - (digit * 100);
			digit = peak / 10;
			GLCD_WriteChar(digit + 0x30);
			peak = peak - (digit * 10);
			digit = peak;
			GLCD_WriteData(0x60);
			GLCD_WriteData(0x60);
			GLCD_WriteChar(digit + 0x30);
		}
		else
		{
			GLCD_GoTo(LOW_COLUMN+position,6);
			digit = peak / 10;
			GLCD_WriteChar(digit + 0x30);
			peak = peak - (digit * 10);
			digit = peak;
			GLCD_WriteData(0x60);
			GLCD_WriteData(0x60);
			GLCD_WriteChar(digit + 0x30);
		}

		GLCD_GoTo(LOW_COLUMN+6+position,7);
		GLCD_WriteData(0xFF);
	}
}

void ReadExtAtoD(void)
{
//	EnableA2D;

	// wait for conversion to finish
	sentinel = 0;
	for(i=0;i<100;i++){
		if( !SPIDataTest ) break;
		delay_ms(1);
		sentinel++;
	}
	// if successful, sentinel will be less than 99
	if( sentinel < 99 ){
		// read high order byte
		for(temp8=0;temp8<8;temp8++){
			SPIClkClear;
			delay_us(100);
			SPIClkSet;
			a2d_h <<= 1;
			if( SPIDataTest ) bit_set(a2d_h,0);
			delay_us(100);
		}
		// read mid byte
		for(temp8=0;temp8<8;temp8++){
			SPIClkClear;
			delay_us(100);
			SPIClkSet;
			a2d_m <<= 1;
			if( SPIDataTest ) bit_set(a2d_m,0);
			delay_us(100);
		}
		// read low byte
		for(temp8=0;temp8<8;temp8++){
			SPIClkClear;
			delay_us(100);
			SPIClkSet;
			a2d_l <<= 1;
			if( SPIDataTest ) bit_set(a2d_l,0);
			delay_us(100);
		}

		//convert to unsigned (toggle msb)
		if( bit_test(a2d_h,5) ) bit_clear(a2d_h,5);
		else bit_set(a2d_h,5);

		// store a/d result in NewCurrentReading, after checking overflow flags
		if( bit_test(a2d_h,6) )
		{
			NewCurrentReading = 1999;
		}
		else if( bit_test(a2d_h,7) )
		{
			NewCurrentReading = 0;
		}
		else 
		{
			NewCurrentReading = (make32(0,(a2d_h&0x3f),a2d_m,a2d_l))>>11;
			if(NewCurrentReading>SelfTestLowerLimit && NewCurrentReading<SelfTestUpperLimit)
				TestResult = Pass;
		}
	}
	else
	{
//			This is an error condition and should be handled
//			For now set reading to 1987
			NewCurrentReading = 1987;
	}

	DisableA2D;

}

void CheckMode(void)
{
	ModeErr = FALSE;
// ddd Debug Dec 21
	if(ACDCPtr == 2)
	{
		if(DCInput > 8)
		{
			if (DCInput > (CurrentReading * 4))
				ModeErr = TRUE;
		}
	}
	else if(ACDCPtr == 4) 
	{
		if(CurrentReading > 2)
		{
			if ((CurrentReading * 4) > DCInput)
				ModeErr = TRUE;
		}
	}
//ddd End Debug  
}


int16 SampleADC(int channel, int samples)
{
	int16 i;
	unsigned int temp;

	set_adc_channel(channel);
	i=0;
	for(temp = 0 ; temp < samples ; temp++)
	{
		i = i + read_adc()/64;
	}
	set_adc_channel(0);
	return(i);
}

void SetRange()
{
	int ButtonPressed;

	RangeCheck();
	if(ModePtr == 7)
	{
		ButtonPressed = 0;
		temp = 100;
		GLCD_ClearScreen();
		Display_Menu(1);
		do{
			if(!SelectButton)
			{
				ButtonPressed++;
				delay_ms(100);
				do{
					//Will need some break out of here
				} while (!SelectButton);
				delay_ms(100);
			}
			//Will need a breakout of here as well			
			delay_ms(100);
			temp--;
			GLCD_GoTo(121,7);
			GLCD_WriteChar(0x30+(temp/10));
			
		} while ( (ButtonPressed == 0) && (temp != 0) );
		RangeCheck();
	}		
}

void RangeCheck()
{
	int32 sum;

	PickUpTestRelay;
	output_high(PIN_E6);
	set_adc_channel(1);

	delay_ms(1000);
	sum=0;
	for(temp = 0 ; temp < 80 ; temp++)
	{
		sum = sum + read_adc()/64;
		delay_ms(1);
	}

	DropOutTestRelay;
	output_low(PIN_E6);

	set_adc_channel(0);
	i = sum/4;

//ddd Feb 1, 2013 Temporarily enable display
//	BatteryDisplay((int32)i, 1);
//	delay_ms(1000);


	if(i < 3800)			//Previous to Ver 8.9 this was 3740
	{
		ModePtr = 1;		//40kV Range
	}
	else if(i < 6400)		//Previous to Ver 8.9 this was 6300
	{
		ModePtr = 2;		//80kV Range
	}
	else if(i < 7500)		//Previous to Ver 8.9 this was 7400
	{
		ModePtr = 3;		//120kV Range
	}
	else if(i < 8100)		//Previous to Ver 8.9 this was 8010
	{
		ModePtr = 4;		//160kV Range
	}
	else if(i < 8500)		//Previous to Ver 8.9 this was 8400
	{
		ModePtr = 5;		//200kV Range
	}
	else if(i < 8800)		//Previous to Ver 8.9 this was 8670
	{
		ModePtr = 6;		//240kV Range
	}
	else
	{
		ModePtr = 7;
	}

}

/*
void CalCheck(int channel)
{
	int16 i;
	unsigned int temp;

	set_adc_channel(channel);
	
	while(Selectbutton)
	{
//		EnableA2D;
//		delay_ms(100);

		i=0;
		for(temp = 0 ; temp < 32 ; temp++)
		{
			i = i + read_adc()/64;
		}
		i=i/32;
		BatteryDisplay((int32)i, 1);

//		ReadExtAtoD();
//		DisableA2D;

//		delay_ms(100);
//		BatteryDisplay((int32)NewCurrentReading, 1);
	}
	set_adc_channel(0);


}

*/