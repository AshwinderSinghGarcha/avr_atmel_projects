//------------------------------------------lcd-----------------------------------// 
//====================== rs = 0 cmd , rs =1 data , e high to low pulse 450nanosec
#define F_CPU 16000000ul
#include <avr/io.h>
#include <util/delay.h>
#define rs 3
#define en 2
#define datapin PORTC
#define cmdpin PORTD

void send_cmd(unsigned char cmd);
void send_data(unsigned char data);
void lcd_intil(void);
void lcd_print(char * str);
void set_cursor(unsigned char x,unsigned char y);
void lcd_clear(void);

//----------------------data pins -----------------------// 
//--------- pb2 pb3 pb4 pb5------mappto
//--------  d4  d5  d6  d7--------------
//-----------------controlling pins-----------------
//----------enable rs  map to pd2 pd3----------



int main(void)
{  
    lcd_intil();
     lcd_print("the world is");
	 _delay_ms(1000);
	lcd_clear();
	//lcd_print('A');
	//set_cursor(1,2);
	//lcd_print('a');
	//_delay_ms(1000);
    while (1) 
    {  send_data(0x41);
		//set_cursor(1,1);
		//lcd_print("the world is");
		//set_cursor(1,2);
		//lcd_print("beautiful");
		//_delay_ms(1000);
    }
}


void send_cmd(unsigned char cmd)
{
	datapin=(cmd & 0xF0);               // high nibble goes first
	cmdpin&=~(1<<rs);                 // rs = 0
	cmdpin|=(1<<en);                 // en high
	_delay_us(1);
	cmdpin&=~(1<<en);                 // en low
	_delay_us(200);
	
	datapin=(cmd<<4);                // low nibble
	cmdpin|=(1<<en);                 // en high
	_delay_us(1);
	cmdpin&=~(1<<en);                 // en low
	_delay_ms(2);
}
void send_data(unsigned char data)
{
	datapin=(data & 0xF0);               // high nibble goes first
	cmdpin|=(1<<rs);                 // rs = 1
	cmdpin|=(1<<en);                 // en high
	_delay_us(1);
	cmdpin&=~(1<<en);                 // en low
	_delay_us(200);
	
	datapin=(data<<4);                // low nibble
	cmdpin|=(1<<en);                 // en high
	_delay_us(1);
	cmdpin&=~(1<<en);                 // en low
	_delay_us(2);
}
void lcd_intil()
{   
	DDRC |=(1<<0)|(1<<1)|(1<<2)|(1<<3);
	//DDRB|=(1<<4)|(1<<5)|(1<<3)|(1<<2);
	DDRD|=(1<<rs)|(1<<en);                      // make ouput port
	_delay_ms(15);
	cmdpin&=~(1<<rs);                           // rs = 0
	cmdpin&=~(1<<en);                           // enable pin =0
	send_cmd(0x33);        // send 33,32 , init , displayon,cursoron
	
	send_cmd(0x32);
	send_cmd(0x28);
	
	send_cmd(0x0E);

	send_cmd(0x01);        // clear lcd
	_delay_ms(2);      //  wait 2 milliseconds
	
}
void lcd_print(char * str)
{
	unsigned char i=0;
	while(str[i]!=0)
	{
		send_data(str[i]);
		_delay_ms(200);
		i++;
	}
}
void set_cursor(unsigned char x,unsigned char y)
{  
	unsigned char commands [] ={0x80,0xC0};
	send_cmd(commands[y-1] + x-1);
	_delay_us(100);
}
void lcd_clear()
{
	send_cmd(0x01);
	_delay_ms(2);
	send_cmd(0x80);
}
