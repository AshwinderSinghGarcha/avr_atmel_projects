/*
 * test.c
 *
 * Created: 10/27/2020 8:28:51 AM
 * Author : Developer
 */ 
#define F_CPU 8000000ul
#include <avr/io.h>
#include <util/delay.h>

int main(void)
{  
	//DDRD =  0b11000000; //making port as o/p 
	DDRD |=  (1<<7)|(1<<6);
	//PORTD = 0b00100000;
	//DDRD &= ~(1<<5);
	PORTD |= (1<<5);
    
    while (1) 
    { 
		if((PIND & (1<<5))==0)
		{PORTD |=(1<<7)|(1<<6);
		
		}
		else
		{
			PORTD &= ~((1<<7)|(1<<6));
			
		}
    }
}

