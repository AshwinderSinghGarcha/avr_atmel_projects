/*
 * timer0 as _pwm.c
 *
 * Created: 10/28/2020 3:50:13 PM
 * Author : ASHWINDER SINGH GARCHA
 */ 
//------------------------------------------------TIMER 0 FAST PWM MODE NON INVERTING ------------
#include <avr/io.h>
#include <util/delay.h>



void PWM()
{
  TCCR0A |= (1<<WGM01)|(1<<WGM00);             // fast pwm mode 
  //TCCR0A |= (1<<COM0A1)|(0<<COM0A0);           // NONINVERTING MODE 
  TCCR0A |= (1<<COM0A1)|(1<<COM0A0);           // INVERTING MODE 
  TCCR0B |= (1<<CS02)|(1<<CS00);               //PRESCALER BY 1024     8000000/1024 = 
  TCNT0  |= 0XFF;
              
}

void PWMout(int duty)
{
	OCR0A = duty;                   // we changing value in ocroa by duty input
}

int main(void)
{  
	PWM();                    // pwm function 
	
	
    DDRD |= (1<<6);          // PD6 AS OUTPUT PWM OC0A 
   
    while (1) 
    {   
		for (int j=0; j<255; j++)
		{
			PWMout(j);
			_delay_ms(1000);
		}
    	for (int j=255; j>0; j--)
		{
			PWMout(j);
			_delay_ms(1000);
		}        
    }         
}

