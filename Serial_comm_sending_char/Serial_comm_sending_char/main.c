

#include <avr/io.h>

#define F_CPU 16000000ul
#define baud 9600
#define X ((F_CPU/16/baud)-1)
#include <util/delay.h>


void uart_setup()
{
 UBRR0H =(X>>8);
 UBRR0L = X;
 UCSR0B |= (1<<TXEN0);
 UCSR0C |= (1<<UPM01)|(1<<UCSZ01)|(1<<UCSZ00);
}

int main(void)
{
    uart_setup();
	char A[11] = {"hello world"};
	
	int i;
	
    while (1) 
    {   
		for(i=0;i<=10;i++)
		{UDR0 = A[i];
		_delay_ms(100);
		}
		if(i>10){i=0;}
    }
}

