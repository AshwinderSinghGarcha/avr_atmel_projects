/*
 * stepper motor.c
 *
 * Created: 10/26/2020 12:43:34 PM
 * Author : Developer
 */ 


#define F_CPU 8000000ul
#include <avr/io.h>
#include <util/delay.h>

//int dt9 =10;   // dont put here//
 //---------------------------------full steps -------------------------------------------
int main()
{   int dt9 =10;              // LARGE STEPPER DELAY
	int dt8 =5;              // SMALL STEPPER DELAY
    DDRB = 0b00001111;  //port b 0,1,2,3 as output port pin 8,9,10,11                bipolar big stepper
	DDRD = 0b11110000;  //port D 7,6,5,4 as output port pin 7,6,5,4                  unipolar small stepper
    while (1) 
    {  
		for (int j=0;j<50;j++)
		    {                                       // clockwise 4 step * 50 times = 200 total 
		    PORTB = 0x06;_delay_ms(dt9);
		    PORTB = 0x05;_delay_ms(dt9);
		    PORTB = 0x09;_delay_ms(dt9);
		    PORTB = 0x0A;_delay_ms(dt9);
		    }
		
		for (int j=0;j<50;j++)
		    {                                                // anticlock wise 200 steps 
			PORTB = 0x0A;_delay_ms(dt9);
			PORTB = 0x09;_delay_ms(dt9);
			PORTB = 0x05;_delay_ms(dt9);
			PORTB = 0x06;_delay_ms(dt9);
			}
		
		for (int j=0;j<50;j++)
		    {                                        // clockwise 8 half step = 4  * 50 times = 200 total
			PORTB = 0x05; _delay_ms(dt9);
			PORTB = 0x01;  _delay_ms(dt9);
			PORTB = 0x09;  _delay_ms(dt9);
			PORTB = 0x08;  _delay_ms(dt9);
			PORTB = 0x0A; _delay_ms(dt9);
			PORTB = 0x02;  _delay_ms(dt9);
			PORTB = 0x06; _delay_ms(dt9);
			PORTB = 0x04; _delay_ms(dt9);
		    }
		
		for (int j=0;j<50;j++)                       // anticlock wise 200 steps
		   {                                        // anticlockwise 8 half step = 4  * 50 times = 200 total
			PORTB = 0x04; _delay_ms(dt9);
			PORTB = 0x06;  _delay_ms(dt9);
			PORTB = 0x02;  _delay_ms(dt9);
			PORTB = 0x0A;  _delay_ms(dt9);
			PORTB = 0x08; _delay_ms(dt9);
			PORTB = 0x09;  _delay_ms(dt9);
			PORTB = 0x01; _delay_ms(dt9);
			PORTB = 0x05; _delay_ms(dt9);
		    }
		
//---------------------------------------------unipolar stepper---------------------------------------------		
		
		
		for (int j=0;j<512;j++)
		{                                       // clockwise 4 step * 50 times = 200 total
			PORTD = 0x80;_delay_ms(dt8);
			PORTD = 0x40;_delay_ms(dt8);
			PORTD = 0x20;_delay_ms(dt8);
			PORTD = 0x10;_delay_ms(dt8);
		}
		
		for (int j=0;j<512;j++)
		{                                                // anticlock wise 200 steps
			PORTD = 0x10;_delay_ms(dt8);
			PORTD = 0x20;_delay_ms(dt8);
			PORTD = 0x40;_delay_ms(dt8);
			PORTD = 0x80;_delay_ms(dt8);
		}
	//---------------------------------------HALF STEPS----	
		for (int j=0;j<512;j++)
		{                                        // clockwise 8 half step = 4  * 50 times = 200 total
			PORTD = 0x90;  _delay_ms(dt8);
			PORTD = 0x80;  _delay_ms(dt8);
			PORTD = 0xA0;  _delay_ms(dt8);
			PORTD = 0x40;  _delay_ms(dt8);
			PORTD = 0x60;  _delay_ms(dt8);
			PORTD = 0x20;  _delay_ms(dt8);
			PORTD = 0x30;  _delay_ms(dt8);
			PORTD = 0x10;  _delay_ms(dt8);
		}
		
		for (int j=0;j<512;j++)                       // anticlock wise 200 steps
		{                                            // anticlockwise 8 half step = 4  * 50 times = 200 total
			PORTD = 0x10;  _delay_ms(dt8);
			PORTD = 0x30;  _delay_ms(dt8);
			PORTD = 0x20;  _delay_ms(dt8);
			PORTD = 0x60;  _delay_ms(dt8);
			PORTD = 0x40;  _delay_ms(dt8);
			PORTD = 0xA0;  _delay_ms(dt8);
			PORTD = 0x80;  _delay_ms(dt8);
			PORTD = 0x90;  _delay_ms(dt8);
		}
		
		
		
		
		
		
    }
}                           

 //---------------------------------half steps -------------------------------------------
/* int main()
 { int dt9 =20;                                                          // speed control
	 DDRB = 0b00001111;  //port b 0,1,2,3 as output port pin 8,9,10,11
	 
	 while (1)
	 {   for (int j=0;j<50;j++)
		 {                                        // clockwise 8 half step = 4  * 50 times = 200 total
		 PORTB = 0x05; _delay_ms(dt9);
		 PORTB = 0x01;  _delay_ms(dt9);
		 PORTB = 0x09;  _delay_ms(dt9);
		 PORTB = 0x08;  _delay_ms(dt9);
		 PORTB = 0x0A; _delay_ms(dt9);
		 PORTB = 0x02;  _delay_ms(dt9);
		 PORTB = 0x06; _delay_ms(dt9);
		 PORTB = 0x04; _delay_ms(dt9);
	    }
	 
	     for (int j=0;j<50;j++)                       // anticlock wise 200 steps
		{                                        // anticlockwise 8 half step = 4  * 50 times = 200 total
			PORTB = 0x04; _delay_ms(dt9);
			PORTB = 0x06;  _delay_ms(dt9);
			PORTB = 0x02;  _delay_ms(dt9);
			PORTB = 0x0A;  _delay_ms(dt9);
			PORTB = 0x08; _delay_ms(dt9);
			PORTB = 0x09;  _delay_ms(dt9);
			PORTB = 0x01; _delay_ms(dt9);
			PORTB = 0x05; _delay_ms(dt9);
		}
     }
 }

*/
