/*
 * serial comm.c
 *
 * Created: 10/30/2020 2:44:12 PM
 * Author : Developer
 */ 

#include <avr/io.h>
#define F_CPU 16000000ul
#define baud 9600
#define BRC ((F_CPU/16/baud)-1)     // 103 decimal = 011001111 value now put this value in UBBR0L UBBR0H 
#include <util/delay.h>

void USART_setup()
{
   UBRR0H = (BRC>>8);              // uuper bit start from 8 th 
   UBRR0L = BRC;                   // lower bit start from 0
   
   UCSR0B |=(1<<TXEN0);            // ENABLE TRANSMITTER 
   UCSR0C |= (1<<UPM01)|(1<<UCSZ01)|(1<<UCSZ00);      // PARITY EVEN , STOP BIT 1 BIT , CHARACTER SIZE 8 BIT 
}



int main(void)
{
    USART_setup();
    while (1) 
    { UDR0 = '8';
		_delay_ms(1000);
		
		/*if ((UDRE0)==1)                          // FLAG WAIT UNTIL SET 
		{
			UDR0 = "HELLO WORLD";
		}*/
    }
}

