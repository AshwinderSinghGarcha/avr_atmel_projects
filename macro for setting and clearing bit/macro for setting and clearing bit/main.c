/*
 * macro for setting and clearing bit.c
 *
 * Created: 10/29/2020 8:40:14 AM
 * Author : Developer
 */ 

#include <avr/io.h>
#include <util/delay.h>

#define setbit(port,bit)   (port) |= (1<<bit)
#define clearbit(port,bit) (port) &= ~(1<<bit)

int main(void)
{
    setbit(DDRD,6) ;
	setbit(DDRD,7)  ;      // set as output 
    while (1) 
    { 
		setbit(PORTD,6);
		clearbit(PORTD,7);
		_delay_ms(4000);
		clearbit(PORTD,6);
		setbit(PORTD,7);
		_delay_ms(4000);
    }
}

