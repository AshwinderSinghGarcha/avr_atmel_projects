/*
 * adc pot input.c
 *
 * Created: 10/30/2020 9:58:07 AM
 * Author : Developer
 */ 
// -----------------------------------------------pot read in adc5 and write to pwm pin pd6------------------------
#include <avr/io.h>
#define F_CPU 16000000ul
#define baud 9600
#define X ((F_CPU/16/baud)-1)
#include <util/delay.h>
#include <stdlib.h>
void uart_setup()                                      //uart setup
{
	UBRR0H =(X>>8);
	UBRR0L = X;
	UCSR0B |= (1<<TXEN0);
	UCSR0C |= (1<<UPM01)|(1<<UCSZ01)|(1<<UCSZ00);
}                        




void pwm_setup()                    // PHASE CORECT PWM 
{ TCCR0A|=(1<<COM0A1)|(1<<WGM00);   // PHASE CORRECT TOP 0XFF
  TCCR0B|=(1<<CS00);                // NO PRESCALING
  TCNT0 = 0XFF;                     // 255 8BIT TIMER
}

void pwm_output(int pot)
{
	OCR0A = pot;                             // OUTPUT PD6
}


void adc_setup()
{
  ADMUX |= (1<<REFS0)|(1<<MUX2)|(1<<MUX0);                         // PORT PC5 ADC5 AND VREF IS AVCC  
  ADCSRA|= (1<<ADEN)|(1<ADIE)|(1<<ADPS2)|(1<<ADPS1)|(1<<ADPS0);    // ADC ENABLE  , ADC INTR EN TELL CONVERSION DONE AND UPDATE DATA REG 
                                                                   // ADC PRESCALER 128 
  DIDR0 |= (1<<ADC5D);                                         // DISABLE DIGITAL INPUT BUFFER FOR ADC5
  
 // ADLAR = 0;
  
}                                                

void adc_start()
{
  ADCSRA|= (1<<ADSC);                       // START CONVERSION
  
}



int main(void)
{    DDRD|= (1<<6);              // LED OCROA ; 
     adc_setup();
	 pwm_setup();
	 uart_setup();
	 
	 
	 
	 uint8_t lower = 0;
	 //uint8_t high = 0;
	 uint8_t POT_read=0;
	 uint16_t adc_result=0;
	 
    while (1) 
    {
	 adc_start();
	 lower=ADCL;
	// high=ADCH;
	 adc_result = ADC;                     // read adc value in 16bit adc varibale
	 
	 
	 POT_read = adc_result/4;
	 pwm_output(POT_read);
	// UDR0 = POT_read;
	 //UDR0 = high;
	 UDR0 = lower;
	// UDR0 = adc_result; 
	 _delay_ms(100);                //reading adc value in serial
		                        // MOVE VALUE OF DATA REG TO PWM_OUTPUT
    }
}

