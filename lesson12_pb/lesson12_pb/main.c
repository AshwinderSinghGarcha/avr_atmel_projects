

//---------------------------bitwise operation by masking bits -----------------
//---------------just use |= for set bit =1 ----------  &= ~ set bit =0---------
//----------------------(1<<7)----meaning move one to 7th position -------------
//----------------(1<<7)|(1<<6)------moving one to 7th and 6th position---------


#define F_CPU 8000000ul
#include <avr/io.h>
#include <util/delay.h>

int main(void)
{
    DDRD |= (1<<7)|(1<<6); // outputport 0b 1100 0000       this is bitwise operaation
	
	DDRD &= ~(1<<5);      // set pin 5 as input
	PORTD|= (1<<5);       // pullup     0b 0001 0000
	
    while (1) 
    {   if ((PIND & (1<<5))==0) // if PIND == 0 looking for pb to connect to gnd from 5 to 0 volts
		
		{
			PORTD |= (1<<7)|(1<<6);        
		}
		else
		{
			PORTD &= ~((1<<7)|(1<<6));
		}
    }
}

