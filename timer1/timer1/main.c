/*
 * timer1.c
 *
 * Created: 10/28/2020 8:34:18 AM
 * Author : Developer
 */ 
//#define F_CPU 16000000UL
#include <avr/io.h>
#include <avr/interrupt.h>

int main(void)

{ 
	DDRD |= (1<<7);   // PD6 OC0A LED TOOGLE MODE AT COMPARE
	
	
   //TCCR0A = 0b01000010;              // COMPARE NON PWM TOGGLE OC0A
 TCCR0A |=(1<<WGM01)     // SET CTC BIT 
	
	//TCCR0B = 0b00000010 ;             // PRESCALER CLK/8 = 1000000 HZ , T= 1 MICROSEC
 TCCR0B |=(1<<CS00)|(1<<CS02);    // PRESCALER 1024
    TCNT0 = 0XFF;        // TIMER 0 WRITE WITH VALUE 255                   TOTAL TIME = 1 *255 =     255 MICROSECONDS 
	//OCR0A = 0XFF;      // TCNT = OCROA THEN GEENRATE FLAG AT 255
	OCR0A = 195;
	//TIMSK0 =0b00000010;    // GENERATE INTERUPT  WE NEED LIBRARY FOR THAT 
 TIMSK0 |= (1<<OCIE0A); 
   sei();     
    while (1) 
    {
		 
    }
	
}
ISR(TIMER0_COMPA_vect)
{
	PORTD ^=(1<<PORTD7);
}