/*
 * timer1.c
 *
 * Created: 10/28/2020 8:34:18 AM
 * Author : Developer
 */ 
//-------------------------------------------timer 0 ctc mode
#define F_CPU 8000000UL


#include <avr/io.h>
#include <avr/interrupt.h>
int LOOP =1;
int MYDELAY = 40 ;              // 40 = 1 SEC 
int main(void)

{ 
	DDRB |= (1<<0);   // PD6 OC0A LED TOOGLE MODE AT COMPARE
	DDRD |=(1<<6);
	
   //TCCR0A = 0b01000010;              // COMPARE NON PWM TOGGLE OC0A
  TCCR0A |=(1<<COM0A0)|(0<<COM0A1)|(1<< WGM01);     // SET CTC BIT 
	
	//TCCR0B = 0b00000010 ;             // PRESCALER CLK/8 = 1000000 HZ , T= 1 MICROSEC
	//TCCR0B |=(1<<CS01);              // PRESCLATER 8
 TCCR0B |=(1<<CS00)|(1<<CS02);    // PRESCALER 1024   = 8000000 / 1024 = 7812.5 HZ ,T=127.99 MICROSEONDS
    //TCNT0 = 0XFF;        // TIMER 0 WRITE WITH VALUE 255                   TOTAL TIME = 1 *255 =     255 MICROSECONDS 
	//OCR0A = 0XFF;      // TCNT = OCROA THEN GEENRATE FLAG AT 255
	OCR0A = 194;         // COMPARE AT 194 = TIME IS   24830.06 MICROSECONDS
	//TIMSK0 =0b00000010;    // GENERATE INTERRUPT  WE NEED LIBRARY FOR THAT 
 TIMSK0 |= (1<<OCIE0A); 
   sei(); 
       
    while (1) 
    {
		asm("nop");
    }
	
}
ISR(TIMER0_COMPA_vect)
{  LOOP++;
	if (LOOP >= MYDELAY )                           //24830.6 *  LOOP 40.27 TIMES  =1 SEC
	{
		PORTB ^= (1<<0);
		LOOP = 1;
	}
	
}