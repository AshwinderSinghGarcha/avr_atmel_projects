/*
 * avr uart.c
 *
 * Created: 2018-08-11 7:14:46 PM
 * Author : ASHWINDER GARCHA
 */ 


#include <avr/io.h>



#define F_CPU 16000000ul
#define BAUD  9600
#define BRC (( F_CPU/16/BAUD)-1)

#include <util/delay.h>
int main (void)
{ 
	/*Set baud rate */

	UBRR0H = (BRC >> 8);
	UBRR0L = BRC;
	//Enable receiver and transmitter */
	UCSR0B = (1<<TXEN0);
	/* Set frame format: 8data, 2stop bit */
	UCSR0C = (1<< UCSZ01)|(1<< UCSZ00);
		while(1)	{		UDR0 = '8';	_delay_ms(1000);		}		}
