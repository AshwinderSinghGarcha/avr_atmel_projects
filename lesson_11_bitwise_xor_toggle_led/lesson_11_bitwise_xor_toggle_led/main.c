/*
 * lesson_11_bitwise_xor_toggle_led.c
 *
 * Created: 10/27/2020 10:54:55 AM
 * Author : Developer
 */ 
#define F_CPU 8000000ul
#include <avr/io.h>
#include <util/delay.h>

int main(void)
{
    DDRD |=(1<<7)|(1<<6);
	
    while (1) 
    { PORTD ^= (1<<7)|(1<<6);
		_delay_ms(500);
    }
}

